Docker Image builds
=====

The image builds found here are for docker based build pipelines like gitlab ci.
 
docker cli image status
---
| image | status |
| --- | --- |
| cli | [![pipeline status](https://gitlab.com/larry1123-builds/docker/badges/cli/pipeline.svg)](https://gitlab.com/larry1123-builds/docker/tree/cli) |
| stable-cli | [![pipeline status](https://gitlab.com/larry1123-builds/docker/badges/stable-cli/pipeline.svg)](https://gitlab.com/larry1123-builds/docker/tree/stable-cli) |
| stable-cli-buildx | [![pipeline status](https://gitlab.com/larry1123-builds/docker/badges/stable-cli-buildx/pipeline.svg)](https://gitlab.com/larry1123-builds/docker/tree/stable-cli-buildx) |

dind image status
---
| image | status |
| --- | --- |
| dind | [![pipeline status](https://gitlab.com/larry1123-builds/docker/badges/dind/pipeline.svg)](https://gitlab.com/larry1123-builds/docker/tree/dind) |
| stable-dind | [![pipeline status](https://gitlab.com/larry1123-builds/docker/badges/stable-dind/pipeline.svg)](https://gitlab.com/larry1123-builds/docker/tree/stable-dind) |
| stable-dind-git | [![pipeline status](https://gitlab.com/larry1123-builds/docker/badges/stable-dind-git/pipeline.svg)](https://gitlab.com/larry1123-builds/docker/tree/stable-dind-git) | 
